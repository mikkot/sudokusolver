# Sudokusolver

### Usage

Call the program and give the sudoku as a string of numbers, with missing fields as zeroes.
All fields must be given (the string must contain 81 characters) for the program to work properly.

```java -jar /sudokusolverjar "001000040000003..."```