package Test;

import Components.Ruudukko;
import Components.RuudukkoKonvertteri;
import Components.Ruutu;
import Components.Sijainti;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RuudukkoTest {
    Ruutu konfliktiRuutu = new Ruutu(new Sijainti(0, 0), 5, false);
    Ruutu konfliktitonRuutu = new Ruutu(new Sijainti(7,8), 7,false);
    String tila = "458927613239861547716345298841532769367489152592716384684173925175294836923658471";
    RuudukkoKonvertteri rk = new RuudukkoKonvertteri();
    Ruudukko ruudukko = new Ruudukko(rk.merkkijonostaPelitilaksi(tila));

    @Test
    public void testHasRowConflict() {
        assertTrue(ruudukko.hasRowConflict(konfliktiRuutu));
        assertFalse(ruudukko.hasRowConflict(konfliktitonRuutu));
    }
    @Test
    public void testHasColumnConflict() {
        assertTrue(ruudukko.hasColumnConflict(konfliktiRuutu));
        assertFalse(ruudukko.hasColumnConflict(konfliktitonRuutu));
    }
    @Test
    public void testHasSquareConflict() {
        assertTrue(ruudukko.hasSquareConflict(konfliktiRuutu));
        assertFalse(ruudukko.hasSquareConflict(konfliktitonRuutu));
    }
}
