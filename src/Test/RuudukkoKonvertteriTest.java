package Test;

import Components.RuudukkoKonvertteri;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RuudukkoKonvertteriTest {
    RuudukkoKonvertteri konvertteri = new RuudukkoKonvertteri();

    @Test
    public void pelitilastaMerkkijonoksiTest() {
        //Should remove all '|', '\s', '\n' and '-' characters from input String
        String input = "0345 4545 || -34356 - | - - 5634 \n 7902 \n --- 32423|4|";
        String output = konvertteri.pelitilastaMerkkijonoksi(input);
        assertEquals("034545453435656347902324234", output);
    }
}
