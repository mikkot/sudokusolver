package Test;

import Components.Alue;
import Components.Ruutu;
import Components.Sijainti;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AlueTest {
    Alue alue = new Alue(new Sijainti(0, 0), new Sijainti(1, 1));

    @Test
    public void testContains() {
        Ruutu ruutu = new Ruutu(new Sijainti(0, 0), 5, false);
        assertTrue(alue.contains(ruutu));
        ruutu.setSijainti(new Sijainti(1,1));
        assertTrue(alue.contains(ruutu));
    }

    @Test
    public void testGetSijainnit() {
        List<Sijainti> sijainnit = List.of(
                new Sijainti(0,0),
                new Sijainti(0,1),
                new Sijainti(1, 0),
                new Sijainti(1,1));
        List<Sijainti> alueenSijainnit = alue.getSijainnit();

        assertEquals(sijainnit.get(0).getX(), alueenSijainnit.get(0).getX());
        assertEquals(sijainnit.get(0).getY(), alueenSijainnit.get(0).getY());

        assertEquals(sijainnit.get(1).getX(), alueenSijainnit.get(1).getX());
        assertEquals(sijainnit.get(1).getY(), alueenSijainnit.get(1).getY());

        assertEquals(sijainnit.get(2).getX(), alueenSijainnit.get(2).getX());
        assertEquals(sijainnit.get(2).getY(), alueenSijainnit.get(2).getY());

        assertEquals(sijainnit.get(3).getX(), alueenSijainnit.get(3).getX());
        assertEquals(sijainnit.get(3).getY(), alueenSijainnit.get(3).getY());
    }
}
