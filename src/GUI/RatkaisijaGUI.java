package GUI;


import Ratkaisija.Ratkaisija;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RatkaisijaGUI  extends JFrame implements ActionListener {
    private Ratkaisija ratkaisija;
    private RuudukkoGUI input;
    private RuudukkoGUI output;
    private InputNappiActionListener inputListener;

    public RatkaisijaGUI() {
        ratkaisija = new Ratkaisija(false);
        inputListener = new InputNappiActionListener();
        initComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel contentPane = new JPanel(new GridLayout());

        //Omat sudoku-ruudukot inputille ja outputille
        this.input = new RuudukkoGUI();
        input.setLayout(new GridLayout(9, 9));
        this.output = new RuudukkoGUI();
        output.setLayout(new GridLayout(9, 9));

        //Lisätään sudoku-ruudukot yläkomponenttiin
        contentPane.add(input);
        contentPane.add(output);

        //Lisätään input napit input-ruudukkoon
        this.addInputButtons(input);
        //Lisätään outputin labelit
        this.addOutputLabels(output);

        //Lisätään aloitusnappi
        JButton ratkaiseNappi = new JButton("Ratkaise");
        ratkaiseNappi.addActionListener(this);
        contentPane.add(ratkaiseNappi);

        this.setContentPane(contentPane);
    }

    private void addInputButtons(RuudukkoGUI input) {
        for(int i = 0; i<9; i++) {
            for(int j = 0; j<9; j++) {
                JButton button = new JButton(Integer.toString(0));
                button.addActionListener(this.inputListener);
                input.add(button);
            }
        }
    }
    private void addOutputLabels(RuudukkoGUI output) {
        for(int i = 0; i<9; i++) {
            for(int j = 0; j<9; j++) {
                JLabel label = new JLabel(Integer.toString(0));
                output.add(label);
            }
        }
    }

    private void ratkaiseJaNayta() {
        String input = this.input.getNappienArvotMerkkijonona();
        this.mappaaRatkaisuOutputLabeleihin(this.ratkaise(input));
    }
    private String ratkaise(String input) {
        return this.ratkaisija.ratkaise(input);
    }
    private void mappaaRatkaisuOutputLabeleihin(String input) {
        Component[] outputLabelit = this.output.getComponents();

        for(int i = 0; i<input.length(); i++) {
            String label = Character.toString(input.charAt(i));
            JLabel labelComponent = (JLabel) outputLabelit[i];
            labelComponent.setText(label);
        }
        //this.output.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.ratkaiseJaNayta();
    }
}
