package GUI;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RuudukkoGUI extends JComponent {
    //Sudoku ruudukko

    public String getNappienArvotMerkkijonona() {
        List<JButton> kaikkiNapit = this.getKaikkiNapit();
        return getNappienTekstitMerkkijonona(kaikkiNapit);
    }

    public List<JButton> getKaikkiNapit() {
        Component[] kaikkiKomponentit = this.getComponents();
        List<JButton> kaikkiNapit = Arrays.stream(kaikkiKomponentit)
                .filter(nappi -> nappi instanceof JButton)
                .map(nappi -> (JButton) nappi)
                .collect(Collectors.toList());

        return kaikkiNapit;
    }

    private String getNappienTekstitMerkkijonona(List<JButton> napit) {
        StringBuilder tulos = new StringBuilder();
        for(JButton nappi : napit) {
            tulos.append(nappi.getText());
        }

        return tulos.toString();
    }
}
