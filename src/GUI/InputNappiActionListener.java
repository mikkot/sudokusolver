package GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InputNappiActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source instanceof JButton) {
            JButton button = (JButton) source;
            int currentButtonValue = Integer.parseInt(button.getText());
            int newValue = currentButtonValue + 1;
            if(newValue > 9) {
                newValue = 0;
            }
            button.setText(Integer.toString(newValue));
        }
        return;
    }
}
