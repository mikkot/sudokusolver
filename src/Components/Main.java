package Components;

import GUI.RatkaisijaGUI;
import Ratkaisija.Ratkaisija;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

public class Main {

    public static void main(String[] args) {
        InputValidaattori validaattori = new InputValidaattori();
        //A valid test sequence: 058000610000060000710305098041000760300000002092000380680103025000090000023000470

        if(args.length == 0) {
            RatkaisijaGUI rGUI = new RatkaisijaGUI();
            rGUI.setVisible(true);
            return;
        }

        String input = args[0];
        if(!validaattori.validoi(input)) { return; }

        Ratkaisija ratkaisija = new Ratkaisija(true);
        ratkaisija.ratkaise(input);
    }
}
