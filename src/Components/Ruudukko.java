package Components;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Ruudukko {
    ArrayList<ArrayList<Ruutu>> peliTila;
    protected final Alue NELIO_1 = new Alue(0,0,2,2);
    protected final Alue NELIO_2 = new Alue(0,3,2,5);
    protected final Alue NELIO_3 = new Alue(0,6,2,8);
    protected final Alue NELIO_4 = new Alue(3,0,5,2);
    protected final Alue NELIO_5 = new Alue(3,3,5,5);
    protected final Alue NELIO_6 = new Alue(3,6,5,8);
    protected final Alue NELIO_7 = new Alue(6,0,8,2);
    protected final Alue NELIO_8 = new Alue(6,3,8,5);
    protected final Alue NELIO_9 = new Alue(6,6,8,8);

    public Ruudukko() {
        this.peliTila = new ArrayList<>(9);
        for(int i = 0; i<9; i++) {
            ArrayList<Ruutu> sarake = new ArrayList<>(9);
            for(int j = 0; j<9; j++) {
                sarake.add(new Ruutu(new Sijainti(i, j), 0, false));
            }
            this.peliTila.add(sarake);
        }
    }

    public Ruudukko(ArrayList<ArrayList<Ruutu>> peliTila) {
        this.peliTila = peliTila;
    }

    public ArrayList<ArrayList<Ruutu>> getPeliTila() {
        return peliTila;
    }

    public Ruutu getRuutu(int x, int y) {
        return peliTila.get(x).get(y);
    }

    public Ruutu getRuutu(Sijainti sijainti) {
        return peliTila.get(sijainti.getX()).get(sijainti.getY());
    }

    public boolean hasConflict(Ruutu ruutu) {
        return hasColumnConflict(ruutu) || hasRowConflict(ruutu) || hasSquareConflict(ruutu);
    }

    public boolean hasColumnConflict(Ruutu ruutu) {
        int row = ruutu.getSijainti().getX();
        List<Ruutu> konfliktit = this.peliTila.get(row)
                .stream()
                .filter(r -> r.getNumero() == ruutu.getNumero())
                .filter(r -> r.getSijainti().compareTo(ruutu.getSijainti()) != 0)
                .collect(Collectors.toList());
        return !konfliktit.isEmpty();
    }

    public boolean hasRowConflict(Ruutu ruutu) {
        int columnSpot = ruutu.getSijainti().getY();
        for(ArrayList<Ruutu> column : this.peliTila) {
            Ruutu verrattava = column.get(columnSpot);
            if(verrattava.getNumero() == ruutu.getNumero()) {
                if(!(verrattava.getSijainti().getX() == ruutu.getSijainti().getX() &&
                        verrattava.getSijainti().getY() == ruutu.getSijainti().getY())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasSquareConflict(Ruutu ruutu) {
        Alue alue = haeRuudunAlue(ruutu);
        List<Sijainti> tarkistettavat = alue.getSijainnit();

        for(Sijainti s : tarkistettavat) {
            if(peliTila.get(s.getX()).get(s.getY()).getNumero() == ruutu.getNumero()) {
                if(ruutu.getSijainti().compareTo(peliTila.get(s.getX()).get(s.getY()).getSijainti()) != 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public Alue haeRuudunAlue(Ruutu ruutu) {
        if(NELIO_1.contains(ruutu)) return NELIO_1;
        if(NELIO_2.contains(ruutu)) return NELIO_2;
        if(NELIO_3.contains(ruutu)) return NELIO_3;
        if(NELIO_4.contains(ruutu)) return NELIO_4;
        if(NELIO_5.contains(ruutu)) return NELIO_5;
        if(NELIO_6.contains(ruutu)) return NELIO_6;
        if(NELIO_7.contains(ruutu)) return NELIO_7;
        if(NELIO_8.contains(ruutu)) return NELIO_8;
        if(NELIO_9.contains(ruutu)) return NELIO_9;
        return null;
    }

    @Override
    public String toString() {
        return tulostaRuudukko();
    }

    private String tulostaRuudukko() {
        StringBuilder tuloste = new StringBuilder();
        for(int i = 0; i<9; i++) {
            if(i == 0 || i == 3 || i == 6) this.tulostaPoikkiviiva(tuloste);
            int jonoNumero = 1;
            tuloste.append("| ");
            for (ArrayList<Ruutu> pylväs : this.peliTila) {
                tuloste.append(pylväs.get(i));
                tuloste.append(" ");
                if(jonoNumero % 3 == 0) tuloste.append("| ");
                jonoNumero++;
            }
            tuloste.append("\n");

        }
        this.tulostaPoikkiviiva(tuloste);
        return tuloste.toString();
    }

    private void tulostaPoikkiviiva(StringBuilder tuloste) {
        for(int j = 0; j<13; j++) {
            tuloste.append("- ");
        }
        tuloste.append("\n");
    }
}
