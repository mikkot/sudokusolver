package Components;

public class Sijainti implements Comparable<Sijainti>{
    int x;
    int y;

    public Sijainti(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean increment() {
        if(this.getX() + 1 < 9) {
            this.x = this.getX() + 1;
            return true;
        } else if(this.getY() + 1 < 9) {
            this.x = 0;
            this.y = this.getY() + 1;
            return true;
        }
        return false;
    }

    public void setX(int x) { this.x = x; }
    public void setY(int y) { this.y = y; }

    @Override
    public int compareTo(Sijainti verrattava) {
        if(this.getY() == verrattava.getY() && this.getX() == verrattava.getX()) return 0;
        if(this.getY() < verrattava.getY() || (this.getX() < verrattava.getX() && this.getY() <= verrattava.getY())) return -1;
        return 1;
    }
}
