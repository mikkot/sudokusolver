package Components;

import java.util.ArrayList;
import java.util.List;

public class RuudukkoKonvertteri {
    public RuudukkoKonvertteri() {

    }

    public String pelitilastaMerkkijonoksi(Ruudukko ruudukko) {
        return pelitilastaMerkkijonoksi(ruudukko.toString());
    }
    public String pelitilastaMerkkijonoksi(String ruudukko) {
        return ruudukko
                .replaceAll("\\|", "")
                .replaceAll("\\s", "")
                .replaceAll("\\n", "")
                .replaceAll("-", "");
    }

    public ArrayList<ArrayList<Ruutu>> merkkijonostaPelitilaksi(String tila) {
        ArrayList<ArrayList<Ruutu>> peliTila = new ArrayList<>(9);
        List<String> sarakkeet = muodostaSarakkeet(tila);

        for(int i = 0; i<9; i++) {
            ArrayList<Ruutu> sarake = new ArrayList<>(9);
            for(int j = 0; j<sarakkeet.size(); j++) {
                int numero = Integer.parseInt(Character.toString(sarakkeet.get(i).charAt(j)));
                Ruutu ruutu = new Ruutu(new Sijainti(i, j), numero, numero != 0);
                sarake.add(ruutu);
            }
            peliTila.add(sarake);
        }

        return peliTila;
    }

    public List<String> muodostaSarakkeet(String tila) {
        ArrayList<String> sarakkeet = new ArrayList(List.of("","","","","","","","",""));
        int sarake = 0;
        for(int i = 0; i<tila.length(); i++) {
            sarakkeet.set(sarake, sarakkeet.get(sarake).concat(Character.toString(tila.charAt(i))));
            sarake++;
            if(sarake >8) sarake = 0;
        }

        return sarakkeet;
    }
}
