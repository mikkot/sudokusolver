package Components;

public class Ruutu {
    int numero;
    Sijainti sijainti;
    boolean annettu;

    public Ruutu(Sijainti sijainti, int numero, boolean annettu) {
        this.sijainti = sijainti;
        this.numero = numero;
        this.annettu = annettu;
    }

    public boolean getAnnettu() {
        return this.annettu;
    }

    public int getNumero() {
        return numero;
    }

    public Sijainti getSijainti() {
        return sijainti;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public boolean increment() {
        if(this.getNumero() + 1 < 10) {
            this.setNumero(this.getNumero() + 1);
            return true;
        }
        this.setNumero(0);
        return false;
    }

    public void setSijainti(Sijainti sijainti) {
        this.getSijainti().setX(sijainti.getX());
        this.getSijainti().setY(sijainti.getY());
    }

    public String toString() {
        return Integer.toString(this.getNumero());
    }
}
