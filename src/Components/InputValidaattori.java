package Components;

public class InputValidaattori {

    public InputValidaattori() {}

    public boolean validoi(String input) {
        if(input.length() < 81) {
            System.out.println("Input too short! Expected: 81 characters, received: " + input.length() + " characters.");
            return false;
        }
        if(input.length() > 81) {
            System.out.println("Input too long! Expected: 81 characters, received: " + input.length() + " characters.");
            return false;
        }
        boolean onlyZeroes = true;
        for(int i = 0; i<input.length(); i++) {
            if(input.charAt(i) != '0') {
                onlyZeroes = false;
                break;
            }
        }
        if(onlyZeroes) {
            System.out.println("Invalid input: contains no given numbers!");
            return false;
        }
        return true;
    }
}
