package Components;

import java.util.ArrayList;
import java.util.List;

public class Alue {
    private Sijainti alku;
    private Sijainti loppu;

    public Alue(Sijainti alku, Sijainti loppu) {
        this.alku = alku;
        this.loppu = loppu;
    }

    public Alue(int ax, int ay, int la, int ly) {
        this.alku = new Sijainti(ax, ay);
        this.loppu = new Sijainti(la, ly);
    }

    public Sijainti getAlku() {
        return alku;
    }

    public Sijainti getLoppu() {
        return loppu;
    }

    public boolean contains(Ruutu ruutu) {
        Sijainti ruudunSijainti = ruutu.getSijainti();
        int ruutuX = ruudunSijainti.getX();
        int ruutuY = ruudunSijainti.getY();
        return ruutuX >= this.getAlku().getX() && ruutuX <= this.getLoppu().getX() && ruutuY >= this.getAlku().getY() && ruutuY <= this.getLoppu().getY();
    }

    public List<Sijainti> getSijainnit() {
        List<Sijainti> kaikkiSijainnitAlueella = new ArrayList<>();

        for(int i = this.getAlku().getX(); i<=this.getLoppu().getX(); i++) {
            for(int j = this.getAlku().getY(); j<=this.getLoppu().getY(); j++) {
                kaikkiSijainnitAlueella.add(new Sijainti(i, j));
            }
        }

        return kaikkiSijainnitAlueella;
    }
}
