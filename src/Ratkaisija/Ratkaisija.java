package Ratkaisija;

import Components.Ruudukko;
import Components.RuudukkoKonvertteri;
import Components.Ruutu;
import Components.Sijainti;

import java.util.ArrayDeque;
import java.util.ArrayList;


public class Ratkaisija {
    private boolean print;

    /**
     *
     * @param print Whether the results are wanted to print in the console
     */
    public Ratkaisija(boolean print) {
        this.print = print;
    }

    /**
     *
     * @param input Pelitila merkkijonona, jossa tyhjät ruudut merkataan nollina, esim. "002350045046080..."
     * @return Palauttaa samantyyppisen merkkijonon kuin ottaa, mutta nollat korvattuina oikeilla numeroilla
     */
    public String ratkaise(String input) {
        RuudukkoKonvertteri konvertteri = new RuudukkoKonvertteri();

        ArrayList<ArrayList<Ruutu>> tila = konvertteri.merkkijonostaPelitilaksi(input);
        Ruudukko ruudukko = new Ruudukko(tila);
        if(print) System.out.println(ruudukko);

        ArrayDeque<Ruutu> edellisetEiAnnetut = new ArrayDeque<>();
        Sijainti nykyinen = new Sijainti(0, 0);
        boolean palaaEdelliseen = false;
        while(nykyinen.getX() < 9 && nykyinen.getY() < 9) {
            Ruutu ruutu;
            //Viimeksi kokeiltu numero ei sopinut, palataan edelliseen käsiteltyyn ruutuun
            if(palaaEdelliseen) {
                palaaEdelliseen = false;
                ruutu = edellisetEiAnnetut.pop();
                if(!ruutu.increment()) {
                    palaaEdelliseen = true;
                    continue;
                } else {
                    while(ruudukko.hasConflict(ruutu)) {
                        if(!ruutu.increment()) {
                            palaaEdelliseen = true;
                            break;
                        }
                    }
                }
                //Asetetaan nykyinen sijainti juuri käydyn ruudun kohdalle, ja sitten inkrementoidan se seuraavaan ja jatketaan looppia
                nykyinen.setX(ruutu.getSijainti().getX());
                nykyinen.setY(ruutu.getSijainti().getY());
                if(!palaaEdelliseen) edellisetEiAnnetut.push(ruutu);
                if(!nykyinen.increment()) break;
                continue;
            }
            ruutu = ruudukko.getRuutu(nykyinen);
            //Ruutu on annettu alkutilassa, siirrytään seuraavaan
            if(ruutu.getAnnettu()) {
                if(!nykyinen.increment()) break;
                continue;
            }
            //Inkrementoidaan ruudun numero, ja jos se menee yli, merkataan flägi palataksemme edelliseen ruutuun
            if(!ruutu.increment()) {
                palaaEdelliseen = true;
                continue;
            } else {
                while(ruudukko.hasConflict(ruutu)) {
                    if(!ruutu.increment()) {
                        palaaEdelliseen = true;
                        break;
                    }
                    if(print) System.out.println(ruudukko);
                }
            }

            //Jos ruutu ei inkrementoidu yli, lisätään ruutu edellisiin käytyihin, jotta voidaan palata takaisinpäin kun kohdataan konflikti
            if(!palaaEdelliseen) edellisetEiAnnetut.push(ruutu);

            //Siirrytään seuraavaan sijaintiin, lopetetaan, jos Sijainti siirtyy ruudukon ulkopuolelle
            if(!nykyinen.increment()) break;
        }

        if(print) System.out.println(ruudukko);


        return konvertteri.pelitilastaMerkkijonoksi(ruudukko);
    }
}
